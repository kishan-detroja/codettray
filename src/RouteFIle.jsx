import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import About from "./Components/About";
import BlogMain from "./Components/Blogs/BlogMain";
import MainLayout from "./Components/Common/MainLayout";
import NoMatch from "./Components/Common/NoMatch";

const RouteFIle = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route path="/blog" element={<BlogMain />} />
          <Route path="/about" element={<About />} />
          <Route path="*" element={<NoMatch />} />
        </Route>
      </Routes>
    </Router>
  );
};

export default RouteFIle;
